<?xml version="1.0" encoding="UTF-8"?>
<!-- copyleft 2023 skøldis -->
<component type="desktop">
  <id>@appid@</id>
  <launchable type="desktop-id">@appid@.desktop</launchable>
  <metadata_license>CC0-1.0</metadata_license>
  <name>Stockpile</name>
  <summary>Keep count of restockable items</summary>
  <project_license>AGPL-3.0-or-later</project_license>
  <description>
    <p>
      Never forget to refill again! Stockpile lets you manage your
      inventory of medication, food, beauty products, or anything else.
      The app will warn you whenever you are short on something,
      so you can obtain more and not have to go empty.
    </p>
  </description>
  <content_rating type="oars-1.1"/>
  <screenshots>
    <screenshot type="default">
      <image>https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/main-view.png</image>
      <caption>The main view of Stockpile</caption>
    </screenshot>
    <screenshot>
      <image>https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/item-view.png</image>
      <caption>The item view of Stockpile</caption>
    </screenshot>
    <screenshot>
      <image>https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/edit-view.png</image>
      <caption>The edit view of Stockpile</caption>
    </screenshot>
  </screenshots>
  <url type="homepage">https://codeberg.org/turtle/stockpile</url>
  <url type="bugtracker">https://codeberg.org/turtle/stockpile/issues</url>
  <url type="vcs-browser">https://codeberg.org/turtle/stockpile</url>
  <url type="translate">https://translate.codeberg.org/engage/jellybean/</url>
  <translation type="gettext">stockpile</translation>
  <releases>
    <release version="0.4.1" date="2024-10-05">
      <description>
        <ul>
          <li translate="no">Updated to GNOME 47 runtime</li>
          <li translate="no">Added Norwegian Bokmål translation</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" date="2023-12-13">
      <description translate="no">
        <ul>
          <li translate="no">Changed app name from “Jellybean” to “Stockpile”</li>
          <li translate="no">Updated icon to match name</li>
          <li translate="no">Reduce user effort by removing unnessescary dialogs and toast</li>
          <li translate="no">Display item amount in the main view</li>
          <li translate="no">Reorder buttons and inputs across the app to make more sense</li>
          <li translate="no">Display item icons on the item page</li>
        </ul>
        <p translate="no">
          If you are having trouble updating the app, please reinstall and
          select to keep your user data, and Stockpile should launch
          correctly afterwards.
        </p>
      </description>
    </release>
    <release version="0.3.1" date="2023-11-18">
      <description translate="no">
        <ul>
          <li translate="no">Added Icelandic translation</li>
          <li translate="no">Fix UI bugs</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" date="2023-11-12">
      <description translate="no">
        <ul>
          <li translate="no">2-pane UI removed to improve clarity of use</li>
          <li translate="no">Added feedback on jellybean delete</li>
          <li translate="no">Icons can now be set for each item</li>
          <li translate="no">Items can now be searched for</li>
          <li translate="no">Fixed various bugs</li>
        </ul>
        <p>
          This release updates the backend. Do not export
          data from this version to another version. If you were using a lower
          version, please run this version first before using a newer version.
        </p>
      </description>
    </release>
    <release version="0.2.1" date="2023-11-06">
      <description translate="no">
        <ul>
          <li translate="no">Added German, Catalan, French, Hungarian, and Italian translations</li>
          <li translate="no">Fix UI bugs</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" date="2023-11-03">
      <description translate="no">
        <ul>
          <li translate="no">Switched to 2-pane interface in larger window sizes</li>
          <li translate="no">Removed destructive action confirmation dialogs, instead replacing them with toasts with an Undo action</li>
          <li translate="no">Added better tooltips to many buttons and improved keyboard shortcuts</li>
          <li translate="no">Removed the “Quick-Use” button</li>
        </ul>
      </description>
    </release>
    <release version="0.1.1" date="2023-10-30">
      <description translate="no">
        <ul>
          <li translate="no">Added localized window title</li>
          <li translate="no">Adjusted icon</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" date="2023-10-29">
      <description translate="no">
        <ul>
          <li translate="no">Added all basic functionality</li>
          <li translate="no">Added translation to Swedish</li>
        </ul>
      </description>
    </release>
  </releases>
  <provides>
    <binary>stockpile</binary>
  </provides>
  <requires>
    <display_length compare="ge">360</display_length>
    <internet>offline-only</internet>
  </requires>
  <developer_name translate="no">Sunniva Løvstad</developer_name>
  <update_contact>stockpile_AT_turtle.garden</update_contact>
</component>

