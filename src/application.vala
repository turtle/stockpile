/* application.vala
 *
 * Copyright 2023-2024 Sunniva Løvstad
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace Stockpile {
    public class Application : Adw.Application {
        public Application () {
            Object (application_id: Config.APP_ID, flags: ApplicationFlags.DEFAULT_FLAGS);
        }

        private string er_flatpak;
        private string systemopplysninger;

        string feilsokning = "OS: %s\nVersion: %s\nArch: %s\n\nGTK: %u.%u.%u (%d.%d.%d)\nlibadwaita: %u.%u.%u (%d.%d.%d)\n\n%s\nProfile: %s\n\nLanguage: %s".printf ( // vala-lint = line-length
            "%s",
            Config.VERSION,
            Config.ARCH,
            Gtk.get_major_version (), Gtk.get_minor_version (), Gtk.get_micro_version (), // Kjørtid
            Gtk.MAJOR_VERSION, Gtk.MINOR_VERSION, Gtk.MICRO_VERSION,                      // Kompileringstid
            Adw.get_major_version (), Adw.get_minor_version (), Adw.get_micro_version (), // Kjørtid
            Adw.MAJOR_VERSION, Adw.MINOR_VERSION, Adw.MICRO_VERSION,                      // Kompileringstid
            "%s", Config.PROFILE,
            Environment.get_variable ("LANG")

            // Versjonene i parentesene og utenfor parentesene skal samsvare. Om de ikke samsvarer,
            // er det noe galt med installasjonen.
        );

        construct {
            Intl.setlocale (LocaleCategory.ALL, "");
            Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.LOCALEDIR);
            Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
            Intl.textdomain (Config.GETTEXT_PACKAGE);

            settings = new GLib.Settings ("garden.turtle.Jellybean");

            ActionEntry[] action_entries = {
                { "about", this.om_aksjon },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", { "<primary>q" }); // ensure styles load

            er_flatpak = Environment.get_variable ("FLATPAK_ID") != null ?
                "Running in flatpak environment\nApp ID: %s".printf (
                    Environment.get_variable ("FLATPAK_ID")) :
                "Running in unsupported environment";

            systemopplysninger = Environment.get_os_info ("PRETTY_NAME") != null ?
                Environment.get_os_info ("PRETTY_NAME") :
                "%s %s".printf (
                    Environment.get_os_info("NAME"),
                    Environment.get_os_info ("VERSION"));

            feilsokning = feilsokning.printf (systemopplysninger, er_flatpak);
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) win = new Stockpile.Window (this);
            win.present ();
            settings.delay ();
        }

        private void om_aksjon () {
            var om = new Adw.AboutDialog () {
                // ingenting her!
                application_name = _("Stockpile"),
                application_icon = Config.APP_ID,
                developer_name = "Sunniva Løvstad",
                version = Config.VERSION,
                developers = {
                    "Sunniva Løvstad <stockpile@turtle.garden>"
                },
                artists = {
                    "Brage Fuglseth https://bragefuglseth.dev"
                },
                copyright = "© 2023-2024 Sunniva Løvstad",
                // Translators: do one of the following, one per line: Your Name, Your Name <email@email.org>, Your Name https://websi.te
                translator_credits = _("translator-credits"),
                license_type = Gtk.License.AGPL_3_0,
                issue_url = "https://codeberg.org/turtle/stockpile/issues/new",
                debug_info = feilsokning,
                debug_info_filename = @"$(Config.APP_ID).txt"
            };

            om.add_credit_section (_("Contributors"), {
                // Contributors: do one of the following, one per line: Your Name, Your Name <email@email.org>, Your Name https://websi.te
            });

            om.add_credit_section (_("Other Helpers"), {
                "Felipe Kinoshita https://felipekinoshita.com/",
                "GNOME App Dev Hangout https://matrix.to/#/%23app-dev:gnome.org"
            });

            if (((Stockpile.Window) this.active_window).is_broken) {
                om.set_debug_info (
                    "%s\n\nBroken data string: %s".printf (
                    om.get_debug_info (), settings.get_string ("jellybeans")
                ));
            }

            om.present (this.active_window);
        }

        protected override void shutdown () {
            settings.apply ();
            base.shutdown ();
        }
    }
}
