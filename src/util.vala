/* util.vala
 *
 * Copyright 2023-2024 Sunniva Løvstad
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
using GLib;

namespace Stockpile {
    public class JellybeanUtil : Object {
        [Description (nick = "Backend object that stores the jellybeans")]
        Stockpile.Jellybeans[] jellybeans_obj = {};
        private Stockpile.Row[]? rows = {};
        private bool[] is_priority = {};
        public signal void status_page_update (bool is_empty, bool is_broken = false);
        public bool is_empty = true;
        public bool is_broken = false;

        construct {
            try {
                this.parse ();
            } catch (Stockpile.ParseError e) {
                status_page_update (false, true);
            }
        }

        private double try_parse_double (string arg) throws Stockpile.ParseError {
            double retur;
            bool lykke;
            lykke = double.try_parse (arg, out retur);
            if (lykke) return retur;
            else throw new Stockpile.ParseError.WRONG_DATA_TYPE ("Not parseable.");
        }

        private int try_parse_int (string arg) throws Stockpile.ParseError {
            int retur;
            bool lykke;
            lykke = int.try_parse (arg, out retur);
            if (lykke) return retur;
            else throw new Stockpile.ParseError.WRONG_DATA_TYPE ("Not parseable.");
        }

        [Description (nick = "Fill in internal jellybean object", blurb = "Parse Jellybean formatted string to Stockpile.Jellybeans[] object")]
        private void parse () throws Stockpile.ParseError {
            // string format inside array: "Item 1,; 50,; Items,; 10,; 2"
            string[] argm;
            Stockpile.Jellybeans jb;
            jellybeans_obj = {};
            if (settings.get_string ("jellybeans") == "") {
                status_page_update (false);
                return;
            }
            is_empty = false;
            string[] parse_args = settings.get_string ("jellybeans").split ("~;");
            debug ("Parsing jellybean-formatted string: " + settings.get_string ("jellybeans"));

            for (int i = 0; i < parse_args.length; i++) {
                argm = parse_args[i].split (",;");
                if (argm.length != 6) {
                    is_empty = true;
                    is_broken = true;
                    status_page_update (false, true);
                    jellybeans_obj = {};
                    throw new Stockpile.ParseError.WRONG_LENGTH (_("There were not enough data points on an item.")); // TODO: tilpasset feiltype
                }

                try {
                    jb = Stockpile.Jellybeans.from_props (
                        desanitize (argm[0]),
                        try_parse_double (argm[1]),
                        desanitize (argm[2]),
                        try_parse_double (argm[3]),
                        try_parse_double (argm[4]),
                        try_parse_int (argm[5])
                    );
                } catch (Stockpile.ParseError e) {
                    is_empty = true;
                    is_broken = true;
                    status_page_update (false, true);
                    jellybeans_obj = {};
                    throw e;
                }

                argm = {};
                jellybeans_obj += jb;
            }

            status_page_update (true);
        }

        [Description (nick = "Add or edit a jellybean", blurb = "Add or edit a jellybean at index. If index is -1, jellybean is a new jellybean, else it is editing a jellybean")]
        public void add_item (int index = -1, Stockpile.Jellybeans jellybean = new Stockpile.Jellybeans ())
        requires (index > -2) {
            if (index == -1)
                jellybeans_obj += jellybean;
            else
                jellybeans_obj[index] = jellybean;

            jellybean_update ();
        }

        [Description (nick = "Delete a jellybean", blurb = "Deletes a jellybean at int index")]
        public void delete_item (int index = 0)
        requires (index > -1) {
            if ((jellybeans_obj.length - 1) == index)
                jellybeans_obj.move (index + 1, index, 1);
            else {
                for (int j = index; j < jellybeans_obj.length; j++)
                    jellybeans_obj.move (j + 1, j, 1);
            }
            jellybeans_obj.resize (jellybeans_obj.length - 1); // TODO: fix memleak
            jellybean_update ();
        }

        [Description (nick = "Returns Jellybeans[] object", blurb = "returns whole jellybeans object, for t.ex. constructing the rows")]
        public Stockpile.Jellybeans[] jellybeans_object () {
            return jellybeans_obj;
        }

        [Description (nick = "Compiles a jellybean object into a storable string")]
        private string jellybean_compile (Stockpile.Jellybeans jlb) {
            string name = sanitize (jlb.name);
            string unit = sanitize (jlb.unit);
            string num = jlb.number.to_string ();
            string lo = jlb.low.to_string ();
            string qui = jlb.quick.to_string ();
            string icon = ((int) jlb.icon).to_string ();
            return @"$name,;$num,;$unit,;$lo,;$qui,;$icon";
        }

        [Description (nick = "Updates the stored string with the current value of the Jellybeans[] array")]
        public void jellybean_update () {
            if (jellybeans_obj.length == 0) {
                settings.set_string ("jellybeans", "");
                is_empty = true;
                is_broken = false;
                status_page_update (false);
                settings.apply ();
                return;
            }
            string retur = jellybean_compile (jellybeans_obj[0]);

            if (jellybeans_obj.length > 1) {
                for (int i = 1; i < jellybeans_obj.length; i++) {
                    retur += "~;";
                    retur += jellybean_compile (jellybeans_obj[i]);
                }
            }

            settings.set_string ("jellybeans", retur);
            is_empty = false;
            settings.apply ();
            debug ("Updated saved jellybean-format string: " + settings.get_string ("jellybeans"));
        }

        [Description (nick = "Updates list of jellybeans", blurb = "Updates list of jellybeans stored in mv_lb and binds show_func to each list item")]
        public void update_rows (Gtk.ListBox mv_lb) {         // memory handling issues here with over 6 jellybeans
            int i = 0;

            mv_lb.remove_all ();
            if (jellybeans_obj.length == 0) {
                status_page_update (false);
                return;
            };
            for (i = 0; i < rows.length; i++) {
                rows[i] = null;
            }
            rows = {};
            is_priority = {};

            for (i = 0; i < jellybeans_obj.length; i++) {
                rows += new Stockpile.Row.from_jellybean (i, jellybeans_obj[i]);
                is_priority += (jellybeans_obj[i].number <= jellybeans_obj[i].low);
            }

            reload_rows (mv_lb);
            status_page_update (true);
        }

        public bool update_row_at_y (Gtk.ListBox mv_lb, int i = -1) {
            if (i == -1) return false; // We should be using the other function for adding items

            // Remove all the rows first
            mv_lb.remove_all ();

            // Destroy this row
            rows[i] = null;

            // Create a new row
            rows[i] = new Stockpile.Row.from_jellybean (i, jellybeans_obj[i]);    // Create the actual row
            is_priority[i] = (jellybeans_obj[i].number <= jellybeans_obj[i].low); // and set its priority

            // Now we can reload the rows. TODO: reload just the one row
            reload_rows (mv_lb);

            return true;
        }

        public void reload_rows (Gtk.ListBox mv_lb /*, bool extras = false, bool is_dim = false */) {
            int j;
            /* if (extras) {
                    if (is_dim) {
                        for (j = 0; j < rows.length; j++) {
                            rows[j].set_dim (true);
                        }
                    } else {
                        for (j = 0; j < rows.length; j++) {
                            rows[j].set_dim (false);
                        }
                    }
            } */
            for (j = 0; j < jellybeans_obj.length; j++) {
                if (is_priority[j]) // && prioritize_ls)
                    mv_lb.prepend (rows[j]);
                else
                    mv_lb.append (rows[j]);
            }
        }

        private string sanitize (string str) {
            return str.replace (";", "&;&").replace (",", "&,&").replace ("~", "&~&");
        }

        private string desanitize (string str) {
            return str.replace ("&;&", ";").replace ("&,&", ",").replace ("&~&", "~");
        }

        public void tilbakestill (Stockpile.Jellybeans[] gjenopprettede = {}) {
            jellybeans_obj = gjenopprettede;
        }
    }
}
