/* util.vala
 *
 * Copyright 2023-2024 Sunniva Løvstad
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
using GLib;

namespace Stockpile {
    public class GjenopprettingsVerktoy : Object {
        private Stockpile.JellybeanUtil gummi_verktoy;
        private Stockpile.Window vindu;
        private string skamfert_streng = "";

        public GjenopprettingsVerktoy.ny (Stockpile.JellybeanUtil verktoy_oppsett, Stockpile.Window vindu_oppsett) {
            gummi_verktoy = verktoy_oppsett;
            vindu = vindu_oppsett;
            skamfert_streng = settings.get_string ("jellybeans");
        }

        private double? try_parse_double (string arg) {
            double retur;
            bool lykke = double.try_parse (arg, out retur);
            if (lykke) return retur;
            return null;
        }

        private int? try_parse_int (string arg) {
            int retur;
            bool lykke = int.try_parse (arg, out retur);
            if (lykke) return retur;
            return null;
        }

        private string desanitize (string str) {
            return str.replace ("&;&", ";").replace ("&,&", ",").replace ("&~&", "~");
        }

        public void spor_om_fjerning () {
            Adw.AlertDialog dialogboks = new Adw.AlertDialog (
                _("Wipe All Data?"),
                _("All of your objects will be permanently deleted."));
            dialogboks.close_response = "avbryt";

            dialogboks.add_response ("avbryt", _("Cancel"));
            dialogboks.add_response ("fjern", _("Wipe"));
            dialogboks.set_response_appearance ("fjern", Adw.ResponseAppearance.DESTRUCTIVE);

            dialogboks.response.connect (svar => {
                if (svar == "avbryt") return;
                settings.set_string ("jellybeans", "");
                gummi_verktoy.tilbakestill ();
                gummi_verktoy.update_rows (vindu.mv_lb);
                gummi_verktoy.status_page_update (false);

                Adw.Toast toast = new Adw.Toast (_("Corrupted data wiped"));
                toast.timeout = 3;
                vindu.legg_til_toast (toast);
            });

            dialogboks.present (vindu);
        }

        public void spor_om_gjenoppretting () {
            Adw.AlertDialog dialogboks = new Adw.AlertDialog (
                _("Recover Data?"),
                _("Stockpile will attempt to recover your data. Items may not be restored correctly, depending on how corrupted the data is."));
            dialogboks.close_response = "avbryt";

            dialogboks.add_response ("avbryt", _("Cancel"));
            dialogboks.add_response ("gjenopprett", _("Recover"));
            dialogboks.set_response_appearance ("gjenopprett", Adw.ResponseAppearance.SUGGESTED);

            dialogboks.response.connect (svar => {
                if (svar == "avbryt") return;

                string[] trinn_ett = skamfert_streng.split ("~;");
                string[] trinn_to;
                string navn;
                int ikon;
                Stockpile.Jellybeans[] returner = {};
                int i = 0;
                for (i = 0; i < trinn_ett.length; i++) {
                    trinn_to = trinn_ett[i].split (",;");
                    trinn_to.resize (6);

                    navn = desanitize (trinn_to[0]);
                    navn = navn == "" ? _("New Item") : navn;

                    ikon = try_parse_int (trinn_to[5]) ?? 0;
                    ikon = ((ikon + 1) % 6) - 1;

                    returner += Stockpile.Jellybeans.from_props (
                        navn,
                        try_parse_double (trinn_to[1]) ?? 50,
                        desanitize (trinn_to[2]),
                        try_parse_double (trinn_to[3]) ?? 10,
                        try_parse_double (trinn_to[4]) ?? 1,
                        ikon
                    );
                }

                gummi_verktoy.tilbakestill (returner);
                gummi_verktoy.update_rows (vindu.mv_lb);
                gummi_verktoy.status_page_update (true);

                Adw.Toast toast = new Adw.Toast (_("Data recovered"));
                toast.timeout = 3;
                vindu.legg_til_toast (toast);
            });

            dialogboks.present (vindu);
        }
    }
}
